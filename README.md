RStudio DarkStyle
=============================================================================

I'm using R in one of my classes and I didn't really like the themes that were
available. I adapted the code from this [post](https://support.rstudio.com/hc/en-us/community/posts/115000437028-Dark-style-for-R-Studio-IDE) so that it would work on my Mac.

It currently looks like this:

![](assets/README-ecf4f4db.png)

I will keep revising and makes fixes as I go along, the more I use it the more issues I'll address!

## Installation Instructions
- Clone this repository
- Go into your "Applications/Rstudio/Contents/". You can get to this by right clicking on "RStudio" and clicking "Show Package Contents"
- Replace the "Resources" folder with the folder you just cloned


RStudio
=============================================================================

RStudio is an integrated development environment (IDE) for the
[R programming language](http://www.r-project.org). Some of its
features include:

- Customizable workbench with all of the tools required to work with R in one
place (console, source, plots, workspace, help, history, etc.).
- Syntax highlighting editor with code completion.
- Execute code directly from the source editor (line, selection, or file).
- Full support for authoring Sweave and TeX documents.
- Runs on all major platforms (Windows, Mac, and Linux) and can also be
run as a server, enabling multiple users to access the RStudio IDE using
a web browser.

For more information on RStudio please visit the
[project website](http://www.rstudio.com/).

Getting the Code
-----------------------------------------------------------------------------

RStudio is licensed under the AGPLv3, the terms of which are included in
the file COPYING. You can find our source code repository on GitHub at [https://github.com/rstudio/rstudio](https://github.com/rstudio/rstudio).

Documentation
-----------------------------------------------------------------------------

For information on how to use RStudio check out our
[online documentation](http://www.rstudio.com/ide/docs/).

For documentation on running your own RStudio Server see the
[server getting started](http://www.rstudio.com/ide/docs/server/getting_started)
guide.

See also the following files included with the distribution:

- COPYING - RStudio license (AGPLv3)
- NOTICE  - Additional open source software included with RStudio
- SOURCE  - How to obtain the source code for RStudio
- INSTALL - How to build and install RStudio from source

If you have problems or want to share feedback with us please visit our
[support website](http://support.rstudio.com/). For other inquiries you can
also email us at [info@rstudio.com](mailto:info@rstudio.com).
